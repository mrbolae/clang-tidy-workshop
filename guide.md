# Setup environment

## Step by step

This tutorial is based on the clang [getting started](https://clang.llvm.org/get_started.html) page. Check out the same branch of llvm, clang and clang-extra-tools to ensure the repositories are compatible.

### Get the sources

#### Clone llvm

```bash
git clone https://github.com/llvm-mirror/llvm.git -b release_40
```

#### Clone clang into llvm

```bash
cd llvm/tools
git clone https://github.com/llvm-mirror/clang.git -b release_40
```

#### Clone extra tools into clang

```bash
cd clang/tools
git clone https://github.com/llvm-mirror/clang-tools-extra.git extra -b release_40
```

### Build clang-tools

#### Setup build directory

```bash
cd ../../../../
mkdir build && cd build
cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../inst -G "Unix Makefiles" ../llvm
```

#### Build & check clang-tools

```bash
make check-clang-tools -j$(nproc)
```

# Useful tools

## Dump AST

Clang is able to dump the AST to inspect certain node types and structures.

```bash
clang -Xclang -ast-dump -fsyntax-only <sourcefile>
#or
clang-check -ast-dump <sourcefile>
```

## AST-Matcher

Here is a reference of the available [AST matchers](http://clang.llvm.org/docs/LibASTMatchersReference.html). To start with have a look in the AST Matcher [tutorial](https://xinhuang.github.io/posts/2015-02-08-clang-tutorial-the-ast-matcher.html).

To play around with the matchers in an interactive environment use clang-query:

```bash
clang-query <file> --
# usage:
clang-query> match <matcher>
```

## run-clang-tidy

This runs the modernize checks for a complete project and fixes all created warnings.

```bash
/usr/lib/llvm-6.0/share/clang/run-clang-tidy.py  -p . -clang-tidy-binary clang-tidy-6.0 -checks="modernize-*,-clang-analyze*,-modernize-make*" -clang-apply-replacements-binary clang-apply-replacements-6.0 -fix
```

The option ```-format``` runs clang-format after the warnings have been fixed.

# cmake support

## Create compilation database

To use the clang tools the compilation commands are needed. Cmake allows to create the compilation database by passing ```-DCMAKE_EXPORT_COMPILE_COMMANDS=ON``` as argument to cmake ([see](https://cmake.org/cmake/help/v3.5/variable/CMAKE_EXPORT_COMPILE_COMMANDS.html)).

## Run clang-tidy when compiling automatically

To run the clang-tidy checks automatically you can use the [builtin functionality of cmake](http://mariobadr.com/using-clang-tidy-with-cmake-36.html).

```cmake
set_target_properties(
  <TARGET_NAME> PROPERTIES
  CXX_CLANG_TIDY <CLANG_TIDY_EXE> "-checks=*,-clang-analyzer-alpha.*"
)
```

## Enable iwyu, clang-tidy, cpplint or cppcheck

Besides clang-tidy other static analyzers and checkers can be use with cmake. Check this [guide](https://blog.kitware.com/static-checks-with-cmake-cdash-iwyu-clang-tidy-lwyu-cpplint-and-cppcheck/) for further information of tools supported and how to use them.

# Write a check from scratch

This section provides a step by step instruction how to develop a new check.
The example check detects the use of the old framework method ```Executor::addExecutor()``` and provides a fix to use the new function ```NewExecutor::defaultExecutor()```.

## Create a new check

```bash
#add_new_check.py <category> <check-name>
add_new_check.py modernize use-new-executor
```

## Update check description

Open rst file 'docs/clang-tidy/checks/<category>-<check-name>.rst' and change the description of the check (incl. detected pattern).

## Add test for check

Open test file 'test/clang-tidy/<category>-<check-name>.cpp'.

```cpp
// RUN: %check_clang_tidy %s modernize-use-new-executor %t

class Executor {
  public:
    Executor() = default;
    void addExecutor() {}
};

class NewExecutor {
  public:
    NewExecutor() = default;
    void defaultExecutor() {}
};

void use_new_executor()
{
    Executor executorOne;
    // CHECK-MESSAGES: :[[@LINE-1]]:5: warning: Use new Executor class [modernize-use-new-executor]
    // CHECK-FIXES: {{^}}    NewExecutor executorOne;{{$}}
    executorOne.addExecutor();
    // CHECK-MESSAGES: :[[@LINE-1]]:17: warning: use defaultExecutor instead of addExecutor [modernize-use-new-executor]
    // CHECK-FIXES: {{^}}    executorOne.defaultExecutor();{{$}}

    // does not trigger the check
    NewExecutor executorTwo;
    executorTwo.defaultExecutor();
}


```

* define the needed classes and the necessairy member function
* add a code snipplet to trigger the check and describe the fix
  * the comment CHECK-MESSAGE describes what kind of message is shown at what position
  * the command CHECK-FIXED describes the line of code after the fix has been applied
* add a code snipplet that does not trigger the check

## Add the AST matcher

Use the test code to create the AST matcher. Open the test file in clang-query ```clang-query test/clang-tidy/modernize-use-new-executor.cpp --```.

Create an AST matcher interactivelly using the matchers provided [here](http://clang.llvm.org/docs/LibASTMatchersReference.html).

```cpp
clang-query> match cxxMemberCallExpr(callee(cxxMethodDecl(hasName("addExecutor"))))
```
The matcher can be improved by checking the class name as well.

```cpp
clang-query> match cxxMemberCallExpr(callee(cxxMethodDecl(hasName("addExecutor"), ofClass(hasName("Executor")))))
```

Move the created matcher to the method ```registerMatchers``` in our check.

```cpp
void UseNewExecutorCheck::registerMatchers(MatchFinder *Finder) {
  Finder->addMatcher(cxxMemberCallExpr(callee(cxxMethodDecl(hasName("addExecutor"), ofClass(hasName("Executor"))))).bind("avoid-addsuccessor"), this);
}
```

This registers the matcher with the label 'avoid-addsuccessor'. In the callback method this label is used to get the matched node from the AST for further processing.

## Implement check

First we need all matching nodes found by the AST matcher.

```cpp
const auto *stmt = Result.Nodes.getNodeAs<CXXMemberCallExpr>("avoid-addsuccessor");
```

In the clang-tidy output the AST matcher points to the object variable instead of the method call. 

```cpp
# note: "root" binds here
    executorOne.addExecutor();
    ^~~~~~~~~~~~~~~~~~~~~~~~~
```

Next a warning message is created which points to the method call. To archieve this the method call (expression) needs to be extracted from the statement.

```cpp
diag(stmt->getExprLoc().getLocWithOffset(0), "use defaultExecutor instead of addExecutor");
```

In order to replace the method call a ```FixItHint``` needs to be created. The first argument defines the ```SourceRange``` to be replaced by the second argument.

```cpp
FixItHint::CreateReplacement(stmt->getExprLoc(), "defaultExecutor");
```

To replace the class from ```Executor``` to ```NewExecutor``` we need to get to the declaration of the variable that calls the member function.

```cpp
const auto *caller = stmt->getImplicitObjectArgument();
const auto *declaration = caller->getReferencedDeclOfCallee();
```

*Workaround:* It was not possible to get the type based on the ```Decl``` class. Therefore a workaround using the ```SourceManager``` is used. The type name is checked in the source file and the end location is calculated.

```cpp
SourceManager &SM = *Result.SourceManager;
auto loc = declaration->getLocStart();
if (StringRef(SM.getCharacterData(loc), strlen("Executor")) != "Executor")
    return;

SourceLocation EndLoc =
  loc.getLocWithOffset(strlen("Executor") - 1);
}
```

And then the type name can be replaced.

```cpp
diag(loc, "Use new Executor class")
    << FixItHint::CreateReplacement(clang::SourceRange(loc, EndLoc), "NewExecutor");
```

The next section shows the complete code snipplet.

```cpp
void UseNewExecutorCheck::check(const MatchFinder::MatchResult &Result) {
    const auto *stmt = Result.Nodes.getNodeAs<CXXMemberCallExpr>("avoid-addsuccessor");
    const auto *caller = stmt->getImplicitObjectArgument();
    const auto *declaration = caller->getReferencedDeclOfCallee();

    SourceManager &SM = *Result.SourceManager;
    auto loc = declaration->getLocStart();
    if (StringRef(SM.getCharacterData(loc), strlen("Executor")) != "Executor")
        return;

    SourceLocation EndLoc =
      loc.getLocWithOffset(strlen("Executor") - 1);

    diag(loc, "Use new Executor class")
        << FixItHint::CreateReplacement(clang::SourceRange(loc, EndLoc), "NewExecutor");

    diag(stmt->getExprLoc().getLocWithOffset(0), "use defaultExecutor instead of addExecutor")
        << FixItHint::CreateReplacement(stmt->getExprLoc(), "defaultExecutor");
}
```

## Test the check

To test the new check call ```make check-clang-tools``` from the build directory.

# Possible clang-tidy checks for hands-on session

## virtual-shadowing

* See [instructions](http://bbannier.github.io/blog/2015/05/02/Writing-a-basic-clang-static-analysis-check.html).

## catch-by-const-ref

* See [instructions](http://blog.audio-tk.com/2018/03/20/writing-custom-checks-for-clang-tidy/).
* Source code is on [github](https://github.com/mbrucher/clang-tools-extra)

## force-pragma-once (live demo)

### Problem

Use `#pragma once` instead of header guards.

### Solution

* Use PPCallback class to be able to evaluate AST before preprocessor is called.
* Use multiple header files to test the check.
* Use of HeaderFileExtentionSet as check argument

### Demo

```bash
#setup project
mkdir build && cd build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..

#execute check
KMX_CHECKS=~/Projects/kmx-clang-tools-extra/inst PATH=${KMX_CHECKS}/bin:${KMX_CHECKS}/share/clang:$PATH run-clang-tidy.py -p . -checks='-*,kmx-force-pragma-once' -header-filter='.h|.hpp|.hxx' -format

#fix issues
<command_from_above> -fix
```

## replace-boost-listof-initialization (live demo)

### Problem

Replace boost initializer list ```boost::assign::list_of<int>(1)(2)(3)(4)``` with c++11 initializer list ```{1, 2, 3, 4}```.

### Solution

* add matcher for boost initializer list
* parse argument in call and surrounding calls
* create std initializer list expression
* generate code with arguments from lexer (before preprocessing starts) to avoid macro and function call replacement
