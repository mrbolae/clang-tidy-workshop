# About

Clang-tidy workshop for bbv Gathering 2018.

# Purpose

* Introduce bbv C++ developers to clang-tidy
* Create own check with clang-tidy

# Prerequisites

* ubuntu >= 16:04
* clang, clang-tidy, clang-tools >= 4.0

# Workshop

* [Introduction](introduction.md)
* [Guide](guide.md)
