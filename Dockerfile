FROM rikorose/gcc-cmake:gcc-8

ARG CLANG_BRANCH=release_40

RUN cd /tmp && \
    git clone -b ${CLANG_BRANCH} http://llvm.org/git/llvm.git && \
    git clone -b ${CLANG_BRANCH} http://llvm.org/git/clang.git && \
    git clone -b ${CLANG_BRANCH} http://llvm.org/git/clang-tools-extra.git && \
    cd llvm/tools && \
    ln -s ../../clang clang && \
    cd ../../clang/tools && \
    ln -s ../../clang-tools-extra extra && \
    cd ../.. && \
    mkdir build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/opt/clang-tools ../llvm && \
    build_cores=$(expr $(nproc) - 1) && \
    make clang -j$build_cores && \
    make check-clang-tools -j$build_cores && \
    mkdir /opt/clang-tools/ && \
    cp -fr bin cmake docs examples include lib share tools utils /opt/clang-tools/

ENV PATH=/opt/clang-tools/bin:$PATH
ENV CC=clang
ENV CXX=clang++
