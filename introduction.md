<!-- $theme: gaia -->
<!-- to create slides use marp(https://yhatt.github.io/marp/) -->

# Clang-Tidy Workshop

##### Michael Burkard @ Gathering 2018

---

## Agenda

* About clang-tidy
* Examples
  * Fix typos
  * Modernize
* Create check

---

## About clang-tidy

* **clang-tidy** is a clang-based C++ linter tool.
* **clang-tidy** provides an extensible framework for diagnosing and fixing typical programming errors.
* **clang-tidy** is modular and provides a convenient interface for *writing new checks*.

---

## Example - fix typos

##### Clang detects error

```bash
clang examples/typos.cpp
# ...
# 3 errors generated.
```

##### Clang-tidy fixes errors

```bash
clang-tidy examples/typos.cpp -fix-errors
# ...
# clang-tidy applied 3 of 3 suggested fixes.
```

---

## Example - modernize

### Build example

```bash
clang++ examples/modernize.cpp
```

### Modernize using clang-tidy

```bash
clang-tidy -checks=modernize* -fix-errors examples/modernize.cpp
# clang-tidy applied 2 of 2 suggested fixes.
```

---

## Create check

* use add_new_check helper
* write test code for check
* create AST matcher based on test code
* register AST matcher
* add warning message
* add fix it hint

---

## Links

* [Clang-tidy website](http://clang.llvm.org/extra/clang-tidy/)
* [Understanding the Clang AST](https://jonasdevlieghere.com/understanding-the-clang-ast/)
* [Matching the Clang AST](https://clang.llvm.org/docs/LibASTMatchers.html)
* [AST Matcher Reference](http://clang.llvm.org/docs/LibASTMatchersReference.html)
