/**
  * Example to demonstrate the clang-tidy modernize checks.
  *
  * Expected fixes:
  * - convert for loop to c++11 style
  */

#include <vector>

int sum(const std::vector<int>& values)
{
    int _sum = 0;
    for(std::vector<int>::const_iterator it = values.begin(); it != values.end(); it++)
    {
        _sum += *it;
    }
    return _sum;
}

int main()
{
    std::vector<int> values = {1, 2, 3};
    int d = sum(values);
}
